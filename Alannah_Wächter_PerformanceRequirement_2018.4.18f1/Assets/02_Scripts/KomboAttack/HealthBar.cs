﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [SerializeField] private Image _healthBar;

    private float _currentHealth;
    private float _damageAmount;

    private void Awake()
    {
        _currentHealth = 1;
    }

    private void OnEnable()
    {
        Events.OnDamage += OnDamageEvent;
    }

    private void OnDisable()
    {
        Events.OnDamage -= OnDamageEvent;
    }

    private void Update()
    {
        _healthBar.fillAmount = _currentHealth;
    }

    private void OnDamageEvent(float amount)
    {
        _currentHealth -= amount;
    }
}
