﻿using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

public class PCController : MonoBehaviour
{
    [Header("Movement")]
    [SerializeField] [Range(1, 10)] private float _movementSpeed;
    [Header("Attack and Kombo")]
    [SerializeField] [Range(0, 1)] private float _registerTime;
    [SerializeField] [Range(1, 5)] private float _komboDistance;
    [Header("UI")]
    [SerializeField] [Range(0, 1)] private float _waitForAttackData;
    [Header("Knock Back")]
    [SerializeField] [Range(1, 50)] private float _knockBackSpeed;
    
    private CharacterController _controller;
    private GameObject _enemy;
    private Animator _anim;

    private bool _frontPunch;
    private bool _backPunch;
    private bool _komboStarter;
    
    private bool _frontPunchKey;
    private bool _backPunchKey;

    private bool _knockBack;

    private OtherUserInterface _ui;

    private void Awake()
    {
        _controller = GetComponent<CharacterController>();
        _ui = FindObjectOfType<OtherUserInterface>();
        _frontPunch = false;
        _backPunch = false;
        _komboStarter = false;
        _knockBack = false;

        _enemy = GameObject.FindWithTag("Enemy");
        _anim = GetComponent<Animator>();
    }

    private void Update()
    {
        _frontPunchKey = Input.GetKeyDown(KeyCode.O);
        _backPunchKey = Input.GetKeyDown(KeyCode.P);
        var direction = new Vector3((Input.GetAxis("Horizontal") * (_movementSpeed / 100)), 0, 0);
        _controller.Move(direction);

        if ((_enemy.transform.position.x - transform.position.x) > _komboDistance)
            return;

        CheckSingleHit();
        CheckKomboHit();

        if (_enemy.transform.position.x >= 7)
            return;

        if (_knockBack)
        {
            _enemy.transform.Translate(
                Vector3.right * (_knockBackSpeed * Time.deltaTime));
        }
    }

    private IEnumerator RegisterAttack(Action<bool> attackType)
    {
        attackType(true);
        yield return new WaitForSeconds(_registerTime);
        attackType(false);
    }
    
    private void CheckSingleHit()
    {
        if (_frontPunchKey && !_frontPunch)
        {
            _anim.SetTrigger("FrontPunch");
            StartCoroutine(RegisterAttack(a=>_frontPunch=a));
            DoDamage(2);
        }

        if (_backPunchKey && !_backPunch)
        {
            _anim.SetTrigger("BackPunch");
            StartCoroutine(RegisterAttack(a=>_backPunch=a));
            DoDamage(3);
        }
    }

    private void CheckKomboHit()
    {
        if (_backPunch && _frontPunchKey)
        {
            StartCoroutine(RegisterAttack(a=>_komboStarter=a));
            DoDamage(1.9f);
        }

        if (_komboStarter && _backPunchKey)
        {
            _anim.SetTrigger("Kombo");
            _ui.SetData("Kombo", _waitForAttackData);
            StartCoroutine(SetKnockBack());
            DoDamage(6.32f);            
        }
    }

    private IEnumerator SetKnockBack()
    {
        _knockBack = true;
        yield return new WaitForSeconds(_registerTime);
        _knockBack = false;
    }

    private void DoDamage(float amount)
    {
        Events.InvokeDamageEvent(amount);
        _ui.SetData(amount.ToString());
    }
}
