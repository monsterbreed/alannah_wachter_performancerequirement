﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OtherUserInterface : MonoBehaviour
{
    [SerializeField] private Text _attackData;

    private CanvasGroup _panel;
    private bool _isVisible;

    private void Awake()
    {
        _panel = GetComponentInChildren<CanvasGroup>();
        _isVisible = false;
        
        _panel.alpha = 0;
        Time.timeScale = 1;
        _isVisible = true;
    }
    public void ShowHideInfo()
    {
        if (_isVisible)
        {
            _panel.alpha = 1;
            Time.timeScale = 0;
            _isVisible = false;
        }
        else
        {
            _panel.alpha = 0;
            Time.timeScale = 1;
            _isVisible = true;
        }
    }

    public void RestartLevel()
    {
        var level = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(level);
    }

    public void SetData(string data, float waitSeconds = 0)
    {
        StartCoroutine(ShowAttackData(data + "!", waitSeconds));
    }
    private IEnumerator ShowAttackData(string data, float waitSeconds)
    {
        var fullAlpha = new Color(_attackData.color.r, _attackData.color.g, _attackData.color.b, 255);
        var zeroAlpha = new Color(_attackData.color.r, _attackData.color.g, _attackData.color.b, 0);
        
        yield return new WaitForSeconds(waitSeconds);
        _attackData.text = data;

        _attackData.CrossFadeAlpha(1, 0, false);
        yield return  new WaitForSeconds(0.3f);
        _attackData.CrossFadeAlpha(0, 1, false);
    }
}
