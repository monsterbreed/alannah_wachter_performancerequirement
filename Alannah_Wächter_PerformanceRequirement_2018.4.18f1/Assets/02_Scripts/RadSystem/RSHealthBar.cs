﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.UI;

public class RSHealthBar : MonoBehaviour
{
    [Header("Progress Bars")]
    [SerializeField] private Image _healthBar;
    [SerializeField] private Image _radBar;
    [Header("Values")]
    [SerializeField] [Range(0, 1)] private float _healAmount;
    [SerializeField] [Range(0, 1)]private float _radAmount;
    [Header("Text Components")]
    [SerializeField] private Text _maxHealthText;
    [SerializeField] private Text _currentHealthText;
    [SerializeField] private Text _currentRadText;
    [Header("Audio")]
    [SerializeField] private AudioSource _source;
    [SerializeField] [Range(1, 10)] private float _maxPitch;
    [SerializeField] [Range(1, 10)]private float _deadPitch;
    [SerializeField] private AudioClip _warnClip;
    [SerializeField] private AudioClip _deadClip;

    private float _currentHealth;
    private float _currentRad;
    private float _maxHealth;

    private void Awake()
    {
        _maxHealth = 1;
        _currentHealth = _maxHealth;
        _currentRad = 0;
    }

    private void Update()
    {
        _healthBar.fillAmount = _currentHealth;
        _radBar.fillAmount = _currentRad;

        _maxHealthText.text = "Max Health: " + Math.Round(_maxHealth*100, 0) + "%";
        _currentHealthText.text = "Current Health: " + Math.Round(_currentHealth*100, 0) + "%";
        _currentRadText.text = "Current Rad: " + Math.Round(_currentRad*100, 0) + "%";
        
        if(Input.GetKey(KeyCode.I))
            IncreaseRad();
        
        else if(Input.GetKeyUp(KeyCode.I))
            SetNewMaxHealth();
        
        else if(Input.GetKey(KeyCode.O))
            DecreaseRad();
        
        else if(Input.GetKeyUp(KeyCode.O))
            SetNewMaxHealth();
        
        else if(Input.GetKey(KeyCode.K))
            IncreaseHealth();
        
        else if(Input.GetKey(KeyCode.L))
            DecreaseHealth();
        
        CheckForSound();
    }

    private void IncreaseRad()
    {
        if (_currentRad >= 1)
            return;
        
        _currentRad += _radAmount * Time.deltaTime;

        if (_currentHealth <= 1f - _currentRad)
            return;
        
        _currentHealth -= _radAmount * Time.deltaTime;
    }

    private void DecreaseRad()
    {
        if (_currentRad <= 0)
            return;
        
        _currentRad -= _radAmount * Time.deltaTime;
    }

    private void IncreaseHealth()
    {
        if (_currentHealth >= _maxHealth)
            return;

        _currentHealth += _healAmount * Time.deltaTime;
    }

    private void DecreaseHealth()
    {
        if (_currentHealth <= 0)
            return;

        _currentHealth -= _healAmount * Time.deltaTime;
    }

    private void SetNewMaxHealth()
    {
        _maxHealth = 1 - _currentRad;
    }

    private void CheckForSound()
    {
        if (_currentHealth <= 0.5f && _currentHealth > 0)
        {
            _source.clip = _warnClip;
            var newPitch = (1 / _currentHealth) / 2 < _maxPitch ? (1 / _currentHealth) / 2 : _maxPitch;
            _source.pitch = newPitch;
            if (!_source.isPlaying)
            {
                _source.Play();
            }
        }
        else if (_currentHealth <= 0 && !_source.isPlaying)
        {
            _source.clip = _deadClip;
            _source.pitch = _deadPitch;
            _source.Play();
        }

        if(_source.clip == _deadClip && _currentHealth > 0)
        {
            _source.Stop();
        }
    }
}
