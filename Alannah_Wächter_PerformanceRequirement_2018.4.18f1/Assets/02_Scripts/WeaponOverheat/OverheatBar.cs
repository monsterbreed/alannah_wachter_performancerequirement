﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = System.Random;

public class OverheatBar : MonoBehaviour
{
    [Header("Overheat")]
    [SerializeField] private Image _overheatBar;
    [Header("All those numbers")]
    [SerializeField][Range(0, 1)] private float _raiseAmount;
    [SerializeField][Range(0, 1)] private float _sinkAmount;
    [Tooltip("Time in seconds you will have to wait until you can shoot again after weapon is overheated.")][SerializeField] private float _overheatTime;
    [Header("Colors")] 
    [SerializeField] private Color _baseColor;
    [SerializeField] private Color _warnColor;
    [SerializeField] private Color _criticalColor;
    [Header("Other")] [SerializeField] private Text _text;

    private float _currentOverheat;
    private bool _isOverheated;

    private void Start()
    {
        _isOverheated = false;
        _overheatBar.fillAmount = 0;
        _currentOverheat = 0;
        _overheatBar.color = _baseColor;
    }
    
    private void Update()
    {
        if (_overheatBar.fillAmount == 1)
        {
            StartCoroutine(WaitForOverheat());
        }

        if (_isOverheated)
        {
            SinkOverheat(1 / _overheatTime);
            _text.text = Math.Round(_currentOverheat * _overheatTime, 1) + "s";
        }
        else
        {
            _text.text = "";
        }

        if(Input.GetMouseButton(0) && !_isOverheated)
            RaiseOverheat();
        
        else if(!Input.GetMouseButton(0) && !_isOverheated)
            SinkOverheat(_sinkAmount);

        _overheatBar.fillAmount = _currentOverheat;
        ChangeBarColor();
    }

    private void RaiseOverheat()
    {
        if (_isOverheated)
            return;
        
        _currentOverheat += _raiseAmount * Time.deltaTime;
    }

    private void SinkOverheat(float amount)
    {
        if (_currentOverheat <= 0)
            return;

        _currentOverheat -= amount * Time.deltaTime;
    }

    private IEnumerator WaitForOverheat()
    {
        _isOverheated = true;
        yield return new WaitForSeconds(_overheatTime);
        _isOverheated = false;
    }

    private void ChangeBarColor()
    {
        if (_isOverheated)
        {
            _overheatBar.color = _criticalColor;
        }
        else if (_currentOverheat == 0)
        {
            _overheatBar.color = _baseColor;
        }
        else if(!_isOverheated && _currentOverheat <= 0.5f)
        {
            _overheatBar.color = Color.Lerp(_baseColor, _warnColor, _overheatBar.fillAmount*2);
        }
        else if (!_isOverheated && _currentOverheat > 0.5f)
        {
            _overheatBar.color = Color.Lerp(_warnColor, _criticalColor, (_overheatBar.fillAmount-0.5f) * 2);
        }
    }
}
