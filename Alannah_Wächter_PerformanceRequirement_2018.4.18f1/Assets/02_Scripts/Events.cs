﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Events
{
    public delegate void DamageEvent(float amount);
    public static event DamageEvent OnDamage;
    
    public static void InvokeDamageEvent(float amount)
    {
        OnDamage?.Invoke(amount/100);
    }
}
